<?php

namespace Drupal\desktime\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Desktime entities.
 *
 * @ingroup desktime
 */
class DesktimeEntityDeleteForm extends ContentEntityDeleteForm {


}
