<?php

namespace Drupal\desktime\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'desktime.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Desktime integration API key.
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#placeholder' => (\Drupal::state()->get('desktime_api_key') ? '*******' : ''),
      '#default_value' => '',
    ];

    $form['desktime_report_days'] = [
      '#type' => 'select',
      '#title' => $this->t('Report last days'),
      '#options' => [
        15 => 15,
        30 => 30,
      ],
      '#default_value' => \Drupal::state()->get('desktime_report_days', 15),
      '#description' => $this->t('This is for the user desktime report page. %path', ['%path' => '/user/%/desktime/report']),
    ];

    $desktime_indicators_users = [];
    if (is_array(\Drupal::state()->get('desktime_indicators_users')) && (count(\Drupal::state()->get('desktime_indicators_users')) > 0)) {
      $desktime_indicators_users = User::loadMultiple(\Drupal::state()->get('desktime_indicators_users'));
    }
    $form['desktime_indicators_users'] = [
      '#type' => 'entity_autocomplete',
      '#title' => t('Users who will be considered for generating general indicators'),
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#default_value' => $desktime_indicators_users,
      '#description' => $this->t('Like the highest productive hours.'),
    ];

    // If you would like to add custom options, you can alter this form and add more options with value in seconds.
    $time_options = [];
    for ($i = 0.5; $i <= 12; $i = $i + 0.5) {
      $time_options[(60 * 60 * $i)] = $this->t('@time hours', ['@time' => $i]);
    }
    // Desktime goals.
    $form['goals'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Goals'),
    ];
    $form['goals']['desktime_goals_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Targeted desk time'),
      '#options' => $time_options,
      '#default_value' => \Drupal::state()->get('desktime_goals_target', (60 * 60 * 7)),
    ];
    $form['goals']['desktime_goals_target_minimum'] = [
      '#type' => 'select',
      '#title' => $this->t('Minimum desk time'),
      '#options' => $time_options,
      '#default_value' => \Drupal::state()->get('desktime_goals_target_minimum', (60 * 60 * 6)),
    ];
    $form['goals']['desktime_goals_productive_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Targeted productive desk time'),
      '#options' => $time_options,
      '#default_value' => \Drupal::state()->get('desktime_goals_productive_target', (60 * 60 * 6)),
    ];
    $form['goals']['desktime_goals_productive_target_minimum'] = [
      '#type' => 'select',
      '#title' => $this->t('Minimum productive desk time'),
      '#options' => $time_options,
      '#default_value' => \Drupal::state()->get('desktime_goals_productive_target_minimum', (60 * 60 * 5)),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update Desktime API key value if provided.
    if ($form_state->getValue('api_key') && (strlen($form_state->getValue('api_key')) > 0)) {
      \Drupal::state()->set('desktime_api_key', $form_state->getValue('api_key'));
    }

    if ($form_state->getValue('desktime_indicators_users') && (count($form_state->getValue('desktime_indicators_users')) > 0)) {
      $selected_users = [];
      foreach($form_state->getValue('desktime_indicators_users') as $value) {
        $selected_users[] = $value['target_id'];
      }
      \Drupal::state()->set('desktime_indicators_users', $selected_users);
    }

    \Drupal::state()->set('desktime_report_days', $form_state->getValue('desktime_report_days'));
    \Drupal::state()->set('desktime_goals_target', $form_state->getValue('desktime_goals_target'));
    \Drupal::state()->set('desktime_goals_target_minimum', $form_state->getValue('desktime_goals_target_minimum'));
    \Drupal::state()->set('desktime_goals_productive_target', $form_state->getValue('desktime_goals_productive_target'));
    \Drupal::state()->set('desktime_goals_productive_target_minimum', $form_state->getValue('desktime_goals_productive_target_minimum'));
  }

}
