<?php

namespace Drupal\desktime\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Desktime revision.
 *
 * @ingroup desktime
 */
class DesktimeEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Desktime revision.
   *
   * @var \Drupal\desktime\Entity\DesktimeEntityInterface
   */
  protected $revision;

  /**
   * The Desktime storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $desktimeEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->desktimeEntityStorage = $container->get('entity_type.manager')->getStorage('desktime');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desktime_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.desktime.version_history', ['desktime' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $desktime_revision = NULL) {
    $this->revision = $this->DesktimeEntityStorage->loadRevision($desktime_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->DesktimeEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Desktime: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Desktime %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.desktime.canonical',
       ['desktime' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {desktime_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.desktime.version_history',
         ['desktime' => $this->revision->id()]
      );
    }
  }

}
