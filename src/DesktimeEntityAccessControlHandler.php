<?php

namespace Drupal\desktime;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Desktime entity.
 *
 * @see \Drupal\desktime\Entity\DesktimeEntity.
 */
class DesktimeEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\desktime\Entity\DesktimeEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished desktime entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published desktime entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit desktime entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete desktime entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add desktime entities');
  }


}
