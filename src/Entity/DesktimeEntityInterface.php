<?php

namespace Drupal\desktime\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Desktime entities.
 *
 * @ingroup desktime
 */
interface DesktimeEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Desktime name.
   *
   * @return string
   *   Name of the Desktime.
   */
  public function getName();

  /**
   * Sets the Desktime name.
   *
   * @param string $name
   *   The Desktime name.
   *
   * @return \Drupal\desktime\Entity\DesktimeEntityInterface
   *   The called Desktime entity.
   */
  public function setName($name);

  /**
   * Gets the Desktime creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Desktime.
   */
  public function getCreatedTime();

  /**
   * Sets the Desktime creation timestamp.
   *
   * @param int $timestamp
   *   The Desktime creation timestamp.
   *
   * @return \Drupal\desktime\Entity\DesktimeEntityInterface
   *   The called Desktime entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Desktime revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Desktime revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\desktime\Entity\DesktimeEntityInterface
   *   The called Desktime entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Desktime revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Desktime revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\desktime\Entity\DesktimeEntityInterface
   *   The called Desktime entity.
   */
  public function setRevisionUserId($uid);

}
