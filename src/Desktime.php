<?php

namespace Drupal\desktime;

use Drupal\user\Entity\User;
use Samaphp\Desktime\Employee;
use Samaphp\Desktime\Helpers;

/**
 * Class Desktime.
 */
class Desktime {

  private $api_key;
  private $employee_id;

  /**
   * The desktime and productive goals.
   *
   * @var array
   */
  private $goals;

  /**
   * Constructs a new Desktime object.
   */
  public function __construct() {
    $this->api_key = \Drupal::state()->get('desktime_api_key');
    $this->employee_id = FALSE;

    $this->setGoals([
      'desktime' => [
        'target' => \Drupal::state()->get('desktime_goals_target', (60 * 60 * 7)),
        'minimum' => \Drupal::state()->get('desktime_goals_target_minimum', (60 * 60 * 6)),
      ],
      'productive' => [
        'target' => \Drupal::state()->get('desktime_goals_productive_target', (60 * 60 * 6)),
        'minimum' => \Drupal::state()->get('desktime_goals_productive_target_minimum', (60 * 60 * 5)),
      ],
    ]);
  }

  /**
   * Set the authorization value.
   */
  public function setCredentials($credentials) {
    $this->api_key = $credentials['api_key'];
    return $this;
  }

  /**
   * Set the goals value.
   */
  public function setGoals($goals) {
    $this->goals = $goals;
    return $this;
  }

  /**
   * Set the Desktime employee ID value.
   */
  public function setEmployeeID($id) {
    $this->employee_id = $id;
    return $this;
  }

  /**
   * Loading the Employee class with child methods.
   *
   * @return \Samaphp\Desktime\Employee
   */
  public function employee() {
    $Employee = new Employee($this->employee_id);
    return $Employee->setCredentials(['api_key' => $this->api_key]);
  }

  /**
   * Loading the Helpers class with employee details.
   *
   * @return \Samaphp\Desktime\Helpers
   */
  public function helpers() {
    $Employee = $this->employee();
    /** @var \Samaphp\Desktime\Helpers $Helpers */
    $Helpers = new Helpers();
    $Helpers->setGoals($this->goals);
    return $Helpers->setEmployee($Employee);
  }

  /**
   * Getting employee report.
   */
  public function reportLastDays($days = 5, $report_type = 'productive', $week_days = [1, 2, 3, 4, 7]) {
    $Helpers = $this->helpers();
    // PHP array of list of dates like 2020-01-22.
    $workdays = $Helpers->getLastDays($days, $week_days);
    return $Helpers->report($workdays, $report_type);
  }

  /**
   * Getting employee report as one line for each dimension.
   */
  public function reportDimensions($report) {
    $Helpers = $this->helpers();
    $data_array = [];

    if (isset($report->pass) && ($report->pass)) {
      $goals = $Helpers->getGoals();
      foreach ($report->data['days'] as $day) {
        // Scaling productive to productive goal.
        list($productive_hours, $productive_minutes) = explode(':', $day['productive']['time'], 2);
        $productive_seconds = (int) $productive_minutes * 60 + (int) $productive_hours * 3600;
        $scaled_productive = ($productive_seconds / $goals['desktime']['target']) * 100;

        $productive_time = explode(':', $day['productive']['time']);
        $productive_time = (int)$productive_time[0] . '.' . $productive_time[1];

        // Scaling desktime to desktime goal.
        list($desktime_hours, $desktime_minutes) = explode(':', $day['desktime']['time'], 2);
        $desktime_seconds = (int) $desktime_minutes * 60 + (int) $desktime_hours * 3600;
        $scaled_desktime = ($desktime_seconds / $goals['desktime']['target']) * 100;

        $desktime_time = explode(':', $day['desktime']['time']);
        $desktime_time = (int)$desktime_time[0] . '.' . $desktime_time[1];

        $data_array[] = [
          'date' => $day['date'],
          'day' => date('D', strtotime($day['date'])),
          'productive' => [
            'scaled' => number_format($scaled_productive),
            'scaled_precious' => number_format($scaled_productive, 2),
            'time' => $productive_time,
            'target' => $day['productive']['target'],
            'minimum' => $day['productive']['minimum'],
          ],
          'desktime' => [
            'scaled' => number_format($scaled_desktime),
            'scaled_precious' => number_format($scaled_desktime, 2),
            'time' => $desktime_time,
            'target' => $day['desktime']['target'],
            'minimum' => $day['desktime']['minimum'],
          ],
        ];
      }
    }

    return $data_array;
  }

  /**
   * Getting employee report as one line for each dimension.
   */
  public function convertReportDimensionsToScore($reportDimensions) {
    $data_array = [
      'productive' => 0,
      'desktime' => 0,
      'combined' => 0,
    ];
    if (is_array($reportDimensions)) {
      $total_days = count($reportDimensions);
      $total_productive = 0;
      $total_desktime = 0;
      foreach ($reportDimensions as $day) {
        $total_productive = $total_productive + $day['productive']['scaled_precious'];
        $total_desktime = $total_desktime + $day['desktime']['scaled_precious'];
      }

      $data_array['productive'] = number_format(($total_productive / ($total_days * 100) * 100), 2);
      $data_array['desktime'] = number_format(($total_desktime / ($total_days * 100) * 100), 2);
      $data_array['combined'] = number_format(($data_array['productive'] + $data_array['desktime']) / 200 * 100, 2);
    }

    return $data_array;
  }

  /**
   * Getting team desktime indicators.
   */
  public function getTeamIndicators() {
    return \Drupal::state()->get('desktime_indicators_stats', [
      'productive' => [
        'time' => 0,
        'user' => NULL,
        'day' => NULL,
      ],
      'desktime' => [
        'time' => 0,
        'user' => NULL,
        'day' => NULL,
      ],
    ]);
  }

  /**
   * Get and save some desktime indicators.
   */
  public function calcTeamIndicators() {
    // Getting selected users for indicators.
    $selected_users = \Drupal::state()->get('desktime_indicators_users');
    $indicators_stats = $this->getTeamIndicators();
    if (is_array($selected_users) && (count($selected_users) > 0)) {
      $desktime_indicators_users = User::loadMultiple(\Drupal::state()->get('desktime_indicators_users'));

      $report_last_days = \Drupal::state()->get('desktime_report_days', 15);
      foreach ($desktime_indicators_users as $user) {
        $employee = $this->employee()->getEmployeeByEmail($user->getEmail());
        if ($employee->pass) {
          $desktime_user = $employee->data;
          $this->setEmployeeID($desktime_user->id);
          $report = $this->reportLastDays($report_last_days);
          if ($report->pass) {
            foreach ($report->data['days'] as $day) {
              if ($day['productive']['time_seconds'] > $indicators_stats['productive']['time']) {
                $indicators_stats['productive'] = [
                  'time' => $day['productive']['time_seconds'],
                  'user' => $user->id(),
                  'day' => $day['date'],
                ];
              }

              if ($day['desktime']['time_seconds'] > $indicators_stats['desktime']['time']) {
                $indicators_stats['desktime'] = [
                  'time' => $day['desktime']['time_seconds'],
                  'user' => $user->id(),
                  'day' => $day['date'],
                ];
              }
            }
          }
        }
      }

      \Drupal::state()->set('desktime_indicators_stats', $indicators_stats);
    }
  }
}
