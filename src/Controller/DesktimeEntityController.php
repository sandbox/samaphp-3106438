<?php

namespace Drupal\desktime\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\desktime\Entity\DesktimeEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DesktimeEntityController.
 *
 *  Returns responses for Desktime routes.
 */
class DesktimeEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Desktime revision.
   *
   * @param int $desktime_revision
   *   The Desktime revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($desktime_revision) {
    $desktime = $this->entityTypeManager()->getStorage('desktime')
      ->loadRevision($desktime_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('desktime');

    return $view_builder->view($desktime);
  }

  /**
   * Page title callback for a Desktime revision.
   *
   * @param int $desktime_revision
   *   The Desktime revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($desktime_revision) {
    $desktime = $this->entityTypeManager()->getStorage('desktime')
      ->loadRevision($desktime_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $desktime->label(),
      '%date' => $this->dateFormatter->format($desktime->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Desktime.
   *
   * @param \Drupal\desktime\Entity\DesktimeEntityInterface $desktime
   *   A Desktime object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DesktimeEntityInterface $desktime) {
    $account = $this->currentUser();
    $desktime_storage = $this->entityTypeManager()->getStorage('desktime');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $desktime->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all desktime revisions") || $account->hasPermission('administer desktime entities')));
    $delete_permission = (($account->hasPermission("delete all desktime revisions") || $account->hasPermission('administer desktime entities')));

    $rows = [];

    $vids = $desktime_storage->revisionIds($desktime);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\desktime\DesktimeEntityInterface $revision */
      $revision = $desktime_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $desktime->getRevisionId()) {
          $link = $this->l($date, new Url('entity.desktime.revision', [
            'desktime' => $desktime->id(),
            'desktime_revision' => $vid,
          ]));
        }
        else {
          $link = $desktime->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.desktime.revision_revert', [
                'desktime' => $desktime->id(),
                'desktime_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.desktime.revision_delete', [
                'desktime' => $desktime->id(),
                'desktime_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['desktime_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
