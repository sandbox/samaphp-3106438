<?php

namespace Drupal\desktime\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ReportController.
 */
class ReportController extends ControllerBase {

  /**
   * Getting the title of user desktime report page.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   *
   * @return array
   *   Return theme.
   */
  public function userReportTitle(AccountInterface $user) {
    return $this->t('User desktime report of @name', ['@name' => $user->getDisplayName()]);
  }

  /**
   * User desktime report.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   *
   * @return array
   *   Return theme.
   */
  public function userReport(AccountInterface $user) {
    $report_last_days = \Drupal::state()->get('desktime_report_days', 15);
    /** @var \Drupal\desktime\Desktime $Employee */
    $Desktime = \Drupal::service('desktime.desktime_service');
    $employee = $Desktime->employee()->getEmployeeByEmail($user->getEmail());
    if ($employee->pass) {
      $desktime_user = $employee->data;

      // Getting current user desktime API key.
      $Desktime->setCredentials(['api_key' => \Drupal::state()->get('desktime_api_key')]);
      $Desktime->setEmployeeID($desktime_user->id);
      $report = $Desktime->reportLastDays($report_last_days);
      $report_dimension = $Desktime->reportDimensions($report);
      $report_score = $Desktime->convertReportDimensionsToScore($report_dimension);
      $report_short = [];
      $report_days = [];
      if ($report->pass) {
        $report_short = $report->data['short'];
        $report_days = clone $report;
        $report_days = $report_days->data['days'];
        krsort($report_days);
      }
      return [
        '#theme' => 'user_desktime_report',
        '#data' => [
          'desktime_user' => $desktime_user,
          'report_days' => $report_days,
          'report_short' => $report_short,
          'report_dimension' => $report_dimension,
          'report_score' => $report_score,
        ],
      ];
    }
    else {
      // Can not load this user correctly from Desktime.
      return [
        '#markup' => $this->t('This user is not registered on Desktime users with his same email here!'),
      ];
    }
  }

  /**
   * User desktime report access callback.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   *
   * @return object
   *   isAllowed() will be TRUE.
   *   isForbidden() will be TRUE.
   */
  public function userReportAccess(AccountInterface $user) {
    // @TODO: Check if current user have needful permission to view desktime.
    /** @var \Drupal\desktime\Desktime $Employee */
    $Desktime = \Drupal::service('desktime.desktime_service');
    $employee = $Desktime->employee()->getEmployeeByEmail($user->getEmail());
    if ($employee->pass) {
      $currentUser = \Drupal::currentUser();
      return AccessResult::allowedIfHasPermission($currentUser, 'desktime view any user reports');
    }

    // The user email does not match any desktime users.
    return AccessResult::forbidden();
  }

}
