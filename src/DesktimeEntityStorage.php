<?php

namespace Drupal\desktime;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\desktime\Entity\DesktimeEntityInterface;

/**
 * Defines the storage handler class for Desktime entities.
 *
 * This extends the base storage class, adding required special handling for
 * Desktime entities.
 *
 * @ingroup desktime
 */
class DesktimeEntityStorage extends SqlContentEntityStorage implements DesktimeEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DesktimeEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {desktime_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {desktime_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
