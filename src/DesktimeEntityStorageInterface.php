<?php

namespace Drupal\desktime;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\desktime\Entity\DesktimeEntityInterface;

/**
 * Defines the storage handler class for Desktime entities.
 *
 * This extends the base storage class, adding required special handling for
 * Desktime entities.
 *
 * @ingroup desktime
 */
interface DesktimeEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Desktime revision IDs for a specific Desktime.
   *
   * @param \Drupal\desktime\Entity\DesktimeEntityInterface $entity
   *   The Desktime entity.
   *
   * @return int[]
   *   Desktime revision IDs (in ascending order).
   */
  public function revisionIds(DesktimeEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Desktime author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Desktime revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
