<?php

/**
 * @file
 * Contains desktime.page.inc.
 *
 * Page callback for Desktime entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Desktime templates.
 *
 * Default template: desktime.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_desktime(array &$variables) {
  // Fetch DesktimeEntity Entity Object.
  $desktime = $variables['elements']['#desktime'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
